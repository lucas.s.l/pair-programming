function safePawns(data) {

    //console.log(data.length)
    ///// BEGIN MATRIX /////

    //declare and initialize variable
    var arrayBoard = []

    //the variable save the number of pieces
    var piecesSave=data.length

    //rows and cols save the numbers of rows and cols of board
    var rows = 8
    var cols = 8

    //console.log(data)

    for( var i = 0; i < rows; i++ ) {
        arrayBoard.push([])
    }

    for (var i = 0; i < rows; i++)
    {
        for (var j = arrayBoard[i].length; j < cols; j++)
        {
            arrayBoard[i].push('X')
        }
    }

    //console.table(arrayBoard)
    ///// END MATRIX /////

    //// BEGIN COORDINATE ////

    //array that has all letter of the alphabet
    var Arrayalphabet = ("abcdefghijklmnopqrstuvwxyz").split('')
    //console.log(Arrayalphabet)

    //loop to travel data and build new matriz with comparisons to Arrayalphabet
    data.forEach((coordinates) => {
        // const [xValue, yValue] = coordinates.split('')
        const xValue = coordinates.split("")[0]
        const yValue = coordinates.split('')[1]

        console.log(`x: ${xValue} y: ${yValue}`)
        //console.log(Arrayalphabet.indexOf(xCoordinate.split("")[0]))

        //loop, if xValue (save the letter a one coordinated) if it matches add 0 to arrayBoard
        Arrayalphabet.forEach((letter, index) => {
            if(xValue === letter) {
                arrayBoard[yValue - 1][index]=0
            }
        })
        // console.log(Arrayalphabet.indexOf(xValues))
    })
    
    //reverse arrayBoard
    console.table(arrayBoard.reverse())

    //loop to revise all elements of array
    arrayBoard.forEach((valueBoardRow, indexBoardRow) => {

        valueBoardRow.forEach((valueBox, indexValueBox) => {

            //if the value is distint a X
            if(valueBox != "X") {

                //if the last row rest one a piecesSave
                if(indexBoardRow === 7) {
                    --piecesSave
                }
                else{

                    //the first column and value indicated is distint a 0 rest one a piecesSave
                    if(indexValueBox === 0 && arrayBoard[indexBoardRow+1][indexValueBox+1] != 0) {
                        --piecesSave
                    }

                    //the last column and value indicated is distint a 0 rest one a piecesSave
                    if(indexValueBox === 7 && arrayBoard[indexBoardRow+1][indexValueBox-1] != 0) {
                        --piecesSave
                    }else {
                        
                        //if the values indicated is distint a 0 rest one a piecesSave
                        if(arrayBoard[indexBoardRow+1][indexValueBox-1] != 0 && arrayBoard[indexBoardRow+1][indexValueBox+1]!=0) {
                            --piecesSave
                        }
                    }   
                }
            }
        })
    })

    //console.log(`Piezas salvadas: ${piecesSave}`)

    return Math.sign(piecesSave) === -1 ? 0 : piecesSave

    //// END COORDINATE ////

}


safePawns(["a2","b4","c6","d8","e1","f3","g5","h8"]) === 7
    ? alert('Test 1 passed!')
    : alert('Test 1 NOT passed!')
     /*safePawns(["b4", "c4", "d4", "e4", "f4", "g4", "e5"]) === 1
     ? alert('Test 2 passed!')
     : alert('Test 2 NOT passed!')*/
     safePawns(["a8","b7","c6","d5","e4","f3","g2","h1"]) === 7
     ? alert('Test 3 passed!')
     : alert('Test 3 NOT passed!')
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!")
